const apiKey = '19196-640ce841-3db6-4a07-a39c-4f2e5a431cb6'

/**
 * Get data from Figma file with levels
 *  @param {Number} levelIndex - Index of loaded level
 * 
 * @returns {Promise}
 */
export default (levelIndex) => {
    return fetch('https://api.figma.com/v1/files/ctLe2tAAYU4s22LlW3ifKJ', {
        method: 'GET',
        headers: {
            'X-Figma-Token': apiKey
        }
    })
    .then(res => {
        return res.json()
    })
    .then(res => {
        return {
            level: createLevel(res, levelIndex),
            maxLevel: getMaxLevel(res)
        }
    })
}

// Working with Figma API
/**
 * 
 * @param {Object} data - JSON represent of Figma file
 * @param {Number} levelIndex - Index of loaded level
 * 
 * @returns {Object}
 */
function createLevel(data, levelIndex) {
    let levelItems = []
    let levelPage = data.document.children.find( page => page.name.toLowerCase() === 'levels')
    let level = levelPage.children.find( page => page.name == levelIndex)
    let levelOffset = level.absoluteBoundingBox
    let blocks = level.children.find( item => item.name.toLowerCase() === 'blocks' ).children
    let lavas = level.children.find( item => item.name.toLowerCase() === 'lavas' ).children
    let coins = level.children.find( item => item.name.toLowerCase() === 'coins' ).children

    blocks.forEach( block => {
        let obj = {
            x: block.absoluteBoundingBox.x - levelOffset.x,
            y: block.absoluteBoundingBox.y - levelOffset.y,
            width: block.absoluteBoundingBox.width,
            height: block.absoluteBoundingBox.height,
        }

        levelItems.push(obj)
    });

    lavas.forEach( lava => {
        let color = lava.fills[0].color

        let obj = {
            x: lava.absoluteBoundingBox.x - levelOffset.x,
            y: lava.absoluteBoundingBox.y - levelOffset.y,
            width: lava.absoluteBoundingBox.width,
            height: lava.absoluteBoundingBox.height,
            color: `rgb(${color.r * 255}, ${color.g * 255}, ${color.b * 255})`,
            type: 'ghost',
            lavaType: lava.name.match(/\[(.*?)\]/)[1]
        }

        levelItems.push(obj)
    });

    coins.forEach( coin => {
        let color = coin.background[0].color

        let obj = {
            x: coin.absoluteBoundingBox.x - levelOffset.x,
            y: coin.absoluteBoundingBox.y - levelOffset.y,
            width: coin.absoluteBoundingBox.width,
            height: coin.absoluteBoundingBox.height,
            color: `rgb(${color.r * 255}, ${color.g * 255}, ${color.b * 255})`,
            type: 'ghost',
            coinType: coin.name.match(/\[(.*?)\]/)[1]
        }

        levelItems.push(obj)
    });

    return levelItems
}

/**
 * 
 * @param {Object} data - JSON represent of Figma file
 * @returns {Number}
 */
function getMaxLevel(data) {
    return data.document.children.find( page => page.name.toLowerCase() === 'levels').children.length
}