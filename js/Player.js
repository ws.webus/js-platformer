import {ctx, gravity, friction} from './GameSettings'

export default class Player {
    constructor(options) {
        this.ctx = ctx
        this.gravity = gravity
        this.friction = friction

        this.width = 15;
        this.height = 15;
        this.color = '#FF6D6D';
        this.x = 90;
        this.y = 390;
        this.prevX = 0;
        this.prevY = 0;
        this.speed = 3;
        this.jumpStrength = 2.4;
        this.velY = 0;
        this.velX = 0;
        this.isJumping = false
        this.isMoving = {
            left: false,
            right: false,
        }
        this.collisionWith = []
        this.score = 0
        this.isOnHeaven = false
        this.enteredDoor = false
        
        Object.assign(this, options)

        this.initEvents()
        this.updateUi()
    }

    // Draw player on canvas
    draw() {
        this.ctx.beginPath()
        this.ctx.rect(this.x, this.y, this.width, this.height)
        this.ctx.fillStyle = this.color
        this.ctx.fill()
        this.ctx.closePath()
    }

    // Update player position
    update() {
        this.prevX = this.x
        this.prevY = this.y
        this.velY += this.gravity
        
        if (this.isMoving.left && this.velX > -this.speed) {
            this.velX -= this.speed
        } else if (this.isMoving.right && this.velX < this.speed) {
            this.velX += this.speed
        }
        
        this.velX *= this.friction
        this.x += this.velX
        this.y += this.velY
        
        this.checkCollision(this.collisionWith)
        this.draw(this.ctx)
    }

    /**
     * Detect collision type and run function depends on block type
     * @param {Array} blocks - Array of blocks to detect collision
     * 
     * if wall -> fix player position
     * if coin -> collect coin and add score to ui
     * if lava -> player go to heaven
     * id door -> player change enteredDoor state to true
     */
    checkCollision(blocks) {
        blocks.forEach( (block, i) => {
            this.isCollided = this.checkIfCollided(this, block)
            if (!this.isCollided) return

            if (block.type === 'wall') {
                this.fixPosition(block)
            } else if (block.coinType && block.coinType === this.type) {
                this.collectCoin()
                this.updateUi()
                blocks.splice(i, 1)
            } else if (block.lavaType && block.lavaType !== this.type) {
                this.toHeaven()
            } else if (block.isDoor) {
                this.enteredDoor = true
            }
        })
    }
    
    /**
     * 
     * @param {Object} a First shape for collision detection
     * @param {Object} b Second shape for collision detection
     */
    checkIfCollided(a, b) {
        return a.x + a.width > b.x &&
                a.x < b.x + b.width &&
                a.y + a.height > b.y &&
                a.y < b.y + b.height
    }

    /**
     * 
     * @param {Object} block Block that collided with player
     */
    fixPosition(block) {
        if (this.prevX >= block.x + block.width) {
            this.x = block.x + block.width
            this.velX = 0
        }

        if (this.prevX + this.width <= block.x) {
            this.x = block.x - this.width
            this.velX = 0
        }

        if (this.prevY + this.height <= block.y) {
            this.y = block.y - this.height
            this.isJumping = false
            this.velY = 0
        }
        if (this.prevY >= block.y + block.height) {
            this.y = block.y + block.height
            this.velY = 0
        }
    }

    // Add scoe
    collectCoin() {
        this.score++
    }

    // Draw score results to UI
    updateUi() {
        this.DOM.score.innerHTML = this.score
    }

    // Player is died
    toHeaven() {
        this.isOnHeaven = true;
    }

    // Add keyboard events to document for player movement
    initEvents() {
        document.addEventListener('keydown', (e) => {
            if (e.keyCode === this.keys.left) {
                this.isMoving.left = true
            } else if (e.keyCode === this.keys.right) {
                this.isMoving.right = true
            } else if (e.keyCode === this.keys.jump) {
                if (!this.isJumping) {
                    this.isJumping = true
                    this.velY = -this.speed * this.jumpStrength
                }
            }
        })

        document.addEventListener('keyup', (e) => {
            if (e.keyCode === this.keys.left) {
                this.isMoving.left = false
            } else if (e.keyCode === this.keys.right) {
                this.isMoving.right = false
            } 
        })
    }
}