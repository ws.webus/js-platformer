import {canvas, ctx} from './GameSettings'
import getLevel from './Levels'
import Player from './Player'
import Block from './Block'

let blocks, player, player2, door, coinsAmount, loopId, maxLevels
let currentLevel = 1;

startGame()

async function startGame() {
    // Creating level blocks
    blocks = await getLevel(currentLevel)
        .then( levelData => {
            maxLevels = levelData.maxLevel
            return levelData.level.map( item => new Block(item) )
        })

    // Count amount of coins to collect
    coinsAmount = blocks.filter( block => block.coinType ).length

    // Create door for next level
    door = new Block({
        x: 50,
        y: 80,
        width: 50,
        height: 60,
        type: 'ghost',
        color: '#38E02F',
        isDoor: true,
        onTop: true
    })

    // Create 1st player 
    player = new Player({
        keys: {
            right: 39,
            left: 37,
            jump: 38
        },
        collisionWith: blocks,
        type: 'red',
        DOM: {
            score: document.querySelector('#p1Score'),
        }
    })

    // Create 2nd player 
    player2 = new Player({
        x: 60, y: 380,
        color: "#01a499",
        keys: {
            right: 68,
            left: 65,
            jump: 87
        },
        color: '#42B3FF',
        collisionWith: blocks,
        type: 'blue',
        DOM: {
            score: document.querySelector('#p2Score'),
        }
    })

    // Start Game loop
    gameLoop()
}

/**
 * Loop ~60 fps
 * 1. Clear canvas
 * 2. Render blocks
 * 3. Update player and player2
 * 4. Check if any of players is died -> GameOver
 * 5. Check of both players score equal total level coins amount -> Open the door to next level
 * 6. Check of both players entert the door for next level -> Restart game
 */
function gameLoop() {
    // 1. Clear canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // 2. Blocks
    blocks.forEach( block => block.draw(ctx) )

    // 3. Player1 & Player2
    player.update()
    player2.update()

    loopId = requestAnimationFrame(gameLoop)

    // 4. Is players on heaven?
    if (player.isOnHeaven || player2.isOnHeaven) {
        gameOver()
        return
    }

    // 5. Is players collect all coins?
    if (player.score + player2.score === coinsAmount) {
        blocks.unshift(door)
    }

    // 6. Are both players evntered next level door?
    if (player.enteredDoor && player2.enteredDoor) {
        nextLevel()
    }
}

/**
 * Cancel gameLoop and restart game
 */
function gameOver() {
    console.log( 'Game over' );
    cancelAnimationFrame(loopId)
    restartGame()
}

/**
 * Cancel gameLoop, Clear all game data and restart
 */
function restartGame() {
    console.log( 'Restart game' );
    resetGameData()
    startGame()
}

function resetGameData() {
    cancelAnimationFrame(loopId)
    blocks = []
    coinsAmount = 0
    door = null
    player = null
    player2 = null
}

/**
 * Reset data
 * Start game
 */
function nextLevel() {
    if (currentLevel === maxLevels) {
        currentLevel = 1
    } else {
        currentLevel++
    }
    resetGameData()
    startGame()
}