let canvas = document.querySelector('canvas'),
    ctx = canvas.getContext('2d'),
    gravity = 0.3,
    friction = 0.8

export {
    canvas,
    ctx,
    gravity,
    friction
}