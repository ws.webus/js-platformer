import {ctx} from './GameSettings'

export default class Block {
    constructor(options) {
        this.ctx = ctx;
        
        this.x = 0;
        this.y = 0
        this.width = 20
        this.height = 20
        this.color = '#1E4167'
        this.type = 'wall'
        this.onTop = false

        Object.assign(this, options)
    }

    draw() {
        this.ctx.beginPath()
        this.ctx.rect(this.x, this.y, this.width, this.height)
        this.ctx.fillStyle = this.color
        this.ctx.fill()
        this.ctx.closePath()
    }
}