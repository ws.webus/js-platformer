let path = require('path');

// module.exports = {
// 	mode: 'development' ,
// 	entry: "./js/main",
// 	output: {
// 		path: path.resolve(__dirname, "js"),
// 		filename: "main.bundle.js"
// 	},
// 	watch: true,
// 	optimization: {
// 		minimize: false
// 	},
// 	devtool: "cheap-eval-source-map"
// };

module.exports = {
	entry: "./js/main",
	output: {
		path: path.resolve(__dirname, "js"),
		filename: "main.bundle.js"
	},
};
